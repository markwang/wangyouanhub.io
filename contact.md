---
layout: page
title: Contact
permalink: /contact/
---

Hello everyone, My name is Wang Youan. If you have difficulty in pronouncing "Youan", you can call me Mark. 

I'm current a PhD candidate in Finance at HKU. I have many interests. If you find something interest, feel free to share with me.

![an image alt text]({{ site.baseurl }}/images/pic.jpg "Wang Youan")

### Contact me

[markwang@connect.hku.hk](mailto:markwang@connect.hku.hk)